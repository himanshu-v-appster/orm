<?php
use Cake\Datasource\ConnectionManager;

ConnectionManager::config('default', [
    'persistent' => false,
    'className' => 'Cake\Database\Connection',
    'driver' => 'Cake\Database\Driver\Mysql',
    'database' => env("DB_DATABASE","db_name"),
    'username' => env("DB_USERNAME","localhost"),
    'password' => env("DB_PASSWORD",""),
    'port' => env("DB_PORT",3306),
    'host' => env("DB_HOST","localhost"),
    'unix_socket' => env("DB_SOCKET",""),
    'cacheMetadata' => false, // If set to `true` you need to install the optional "cakephp/cache" package.
    'quoteIdentifiers' => true
]);

\Cake\ORM\TableRegistry::config('Authors',[
    'className' => 'App\Model\Table\AuthorsTable',
    'entityClass' => 'App\Model\Entity\Author',
]);
\Cake\ORM\TableRegistry::config('Categories',[
    'className' => 'App\Model\Table\CategoriesTable',
    'entityClass' => 'App\Model\Entity\Category',
]);
\Cake\ORM\TableRegistry::config('Books',[
    'className' => 'App\Model\Table\BooksTable',
    'entityClass' => 'App\Model\Entity\Book',
]);