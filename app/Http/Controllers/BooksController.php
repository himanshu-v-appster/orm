<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cake\ORM\TableRegistry;
class BooksController extends Controller
{
    public function index(){
        $booksTbl = TableRegistry::get("Books");
        $books = $booksTbl->find()->contain([
            "Authors",
            "Categories"
        ])->where(["Books.id <" => 30]);
        
        return view("Books.index")->with("books",$books);
    }
}
